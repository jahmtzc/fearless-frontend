import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = presentation;
        data.max_attendees = attendee;
        data.location = location;

        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferencesUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setAttendee('');
            setPresentation('');
            setLocation('');
            }
    }
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState("");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [start, setStart] = useState('');
    const handleStartChange = (event) =>{
        const value = event.target.value;
        setStart(value);
    }
    const [end, setEnd] = useState('');
    const handleEndChange = (event) =>{
        const value = event.target.value;
        setEnd(value);
    }
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) =>{
        const value = event.target.value;
        setDescription(value);
    }
    const [presentation, setPresentation] = useState('');
    const handlePresentationChange = (event) =>{
        const value = event.target.value;
        setPresentation(value);
    }
    const [attendee, setAttendee] = useState('');
    const handleAttendeeChange = (event) =>{
        const value = event.target.value;
        setAttendee(value);
    }
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) =>{
        const value = event.target.value;
        setLocation(value);
    }
    const fetchData = async () => {

    const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={start} onChange={handleStartChange} placeholder="Starts" required type="date" name="starts" id="start-date" className="form-control" />
                <label htmlFor="start-date">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={end} onChange={handleEndChange} placeholder="Ends" required type="date" name="ends" id="end-date" className="form-control" />
                <label htmlFor="end-date">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} required type="text" name="description" id="description" className="form-control" rows="5"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={presentation} onChange={handlePresentationChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={attendee} onChange={handleAttendeeChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
